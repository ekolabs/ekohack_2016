'use strict';

var gulp = require('gulp-help')(require('gulp'), {
    hideEmpty: true,
    hideDepsMessage: true
});

var pkg = require('./package.json');
var projectKit = require('@ekolabs/project-kit');
var gulpConfig  = require('./gulpconfig.json');

// Initialize project kit
gulp = projectKit.init(gulp, pkg, gulpConfig);

// Add any gulp tasks overrides added specific to this project.
require('./gulpoverrides.js');
