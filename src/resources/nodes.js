define(['resources/ivds'], function(ivds) {
    return [{
        id: 'intro',
        source: ivds.vid_video_a1_1otgrakytuxw4g0sso0kg4ossk04o,
        data: {
            decision: {
                startTime: 7.66,
                endTime: 11.22,
                children: ['brunette', 'blonde'],
                defaults: ['brunette', 'blonde'],
            }
        }
    }, {
        id: 'brunette',
        source: ivds.vid_video_b1_1otgrajex2g0ogcgko4w88s0ggw44,
        data: {
            decision: {
                startTime: 6.06,
                endTime: 13.06,
                children: ['rts'],
                defaults: ['rts']
            }
        }
    }, {
        id: 'blonde',
        source: ivds.vid_video_b2_1otgr9rkk04k8gokkg0kwkss8gws0,
        data: {
            decision: {
                startTime: 6.06,
                endTime: 13.06,
                children: ['rts'],
                defaults: ['rts']
            }
        }
    }, {
        id: 'rts',
        source: ivds.vid_video_9e01435adf2ee4e00ea2bc65fb8c779f_1otgrdrjm8e80o8ggk04osogwk4gc,
        data: {
            decision: {
                startTime: 1,
                endTime: 14.43,
                children: ['intro'],
                defaults: []
            }
        }
    }];
});